//
//  ViewController.swift
//  PageViewController
//
//  Created by Amol Gaikwad on 3/6/17.
//  Copyright © 2017 Amol Gaikwad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
     @IBOutlet weak var pageControl: UIPageControl!
    var pageViewController : UIPageViewController?
    var images = ["image1", "image2", "image3"]
    
    //
    var pendingIndex : Int?
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        createPageViewController()
    }
    
    func createPageViewController() {
        //Instantiate the page view controller
        
        let pageController = self.storyboard?.instantiateViewController(withIdentifier: "pageVC") as! UIPageViewController
        pageController.dataSource = self
        pageController.delegate = self
        
        if images.count > 0 {
            let contentController = getContentViewController(withIndex: 0)!
            let contentControllers = [contentController]
        
            pageController.setViewControllers(contentControllers, direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }
        
        pageViewController = pageController
        
        self.addChildViewController(pageViewController!)
        
        self.view.addSubview(pageViewController!.view)
        
        pageViewController?.didMove(toParentViewController: self)
    
        
    }
    
    func getContentViewController(withIndex index : Int) -> ContentViewController? {
        
        if index < images.count {
            let contentVC = self.storyboard?.instantiateViewController(withIdentifier: "contentVC") as! ContentViewController
            contentVC.itemIndex = index
            contentVC.imageName = images[index]
            return contentVC
        }
        return nil
    }
    
}

extension ViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    // MARK :- UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let contentVC = viewController as! ContentViewController
        if contentVC.itemIndex > 0 {
            return getContentViewController(withIndex: contentVC.itemIndex - 1)
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let contentVC = viewController as! ContentViewController
        
        if contentVC.itemIndex + 1 < images.count {
            return getContentViewController(withIndex: contentVC.itemIndex + 1)
        }
        return nil
    }
    
    // MARK :- UIPageViewControllerDelegate
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        pendingIndex = (pendingViewControllers.first as! ContentViewController).itemIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            if let index = pendingIndex {
                self.pageControl.currentPage = index
            }
        }
    }
}

