//
//  ContentViewController.swift
//  PageViewController
//
//  Created by Amol Gaikwad on 3/6/17.
//  Copyright © 2017 Amol Gaikwad. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var itemIndex : Int = 0
    var imageName : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentImage = imageName {
            imageView.image = UIImage(named: currentImage)
        }

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
